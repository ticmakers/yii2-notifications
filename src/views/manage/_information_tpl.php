<div class="card h-100 p-3">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6">
            {{model.type:1}}
        </div>
        <div class="col-12 col-sm-6 col-md-6">
            {{model.title:2}}
        </div>
        <div class="col-12">
            {{model.message:3}}
        </div>
        <div class="col-12">
            {{model.additional_information:4}}
        </div>
        <div class="col-12">
            {{model.buttons:5}}
        </div>
        <div class="col-12 col-sm-6 col-md-6">
            {{model.small_icon:6}}
        </div>
        <div class="col-12 col-sm-6 col-md-6">
            {{model.big_icon:7}}
        </div>
        <div class="col-12 col-sm-6 col-md-6">
            {{model.generated_from:8}}
        </div>
    </div>

</div>