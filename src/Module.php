<?php

namespace ticmakers\notifications;

use Yii;
use ticmakers\notifications\models\base\Notifications;
use ticmakers\notifications\models\base\NotificationRecipients;
use yii\base\Model;
use yii\db\BaseActiveRecord;

/**
 * notification module definition class
 */
class Module extends \ticmakers\core\base\Module/* \yii\base\Module */
{
    /**
     * @inheritDoc
     */
    public $defaultRoute = 'manage';

    /**
     * Configuiraciones de onesignal por defecto.
     *
     *  @var array
     */
    public $onesignalConfig = [];

    /**
     * Configuraciones de mailer por defecto.
     *
     * @var array
     */
    public $mailerConfig = [];

    /**
     * Configuraciones para mailrelay por defecto.
     *
     * @var array
     */
    public $mailrelayConfig = [];

    /**
     * Permite definir el metodo de envio para las notificaciones por correo electronico
     *
     * @var string
     */
    public $methodSendMail = 'smtp';

    /**
     * Undocumented variable
     *
     * @var array
     */
    public $channels = [
        'smtp' => [
            'class' => 'ticmakers\notifications\components\channels\EmailChannel'
        ],
        'onesignal' => [
            'class' => 'ticmakers\notifications\components\channels\OnesignalChannel'
        ],
        'mailrelay' => [
            'class' => 'ticmakers\notifications\components\channels\MailRelayChannel'
        ],
        'sms' => [
            'class' => 'ticmakers\notifications\components\channels\SMSChannel'
        ],
        'web' => [
            'class' => 'ticmakers\notifications\components\channels\WebChannel'
        ]
    ];

    /**
     * Permite establecer las migas de pan base para el módulo
     *
     * @var array
     */
    public $breadcrumbsBase;

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'ticmakers\notifications\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (empty($this->breadcrumbsBase)) {
            $this->breadcrumbsBase = [];
        }
        // inicializa el módulo con la configuración cargada desde config.php
        \Yii::configure($this, require __DIR__ . '/config/main.php');
        $this->registerTranslations();
        $this->validateProperties();
    }

    /**
     * Registra las traducciones para los mensajes de todo el módulo
     *
     * @return void
     */
    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations["{$this->id}*"])) {
            Yii::$app->i18n->translations["{$this->id}*"] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => __DIR__ . '/messages',
                'fileMap' => [
                    'notifications' => 'notifications.php',
                ],
            ];
        }
    }

    /**
     * Realiza la validación de las configuraciones necesarias para el módulo
     *
     * @return void
     */
    public function validateProperties()
    { }

    /**
     * Obtiene la instancia del canal el cual se utilizará para enviar la notificación
     *
     * @return Channel $channelInstance
     */
    public function getChannel($channelType, $config = [])
    {
        $channelObject = null;
        $channelConfig = null;
        if ($channelType == 'E') {
            switch ($this->methodSendMail) {
                case 'mailrelay':
                    $userConfiguration = array_merge($this->channels['mailrelay'], $config);
                    $channelConfig = array_merge($this->mailrelayConfig, $userConfiguration);
                    break;
                case 'swiftmailer':
                default:
                    $userConfiguration = array_merge($this->channels['smtp'], $config);
                    $channelConfig = $userConfiguration;
                    break;
            }
        } else {
            foreach ($this->channels as $channel) {
                $channelInstance = Yii::createObject($channel);
                if ($channelInstance->type == $channelType) {
                    $channelConfig = array_merge($channel, $config);
                    break;
                }
            }
        }
        $channelObject = Yii::createObject($channelConfig);
        if ($channelObject) {
            return $channelObject;
        }
        throw new \yii\base\UnknownClassException(Yii::t($this->id, "No channel found for the notification type."));
    }

    /**
     * Realiza un envío rápido de una notficiación
     * 
     * @param string $typeChannel Tipo de canal que se querrá utilizar.
     * @param array $options Opciones como configuración y los datos para las notificaciones.
     * @return boolean
     */
    public function send($typeChannel, $options = [])
    {
        $response = false;
        $configuration = (!empty($options) && isset($options['config'])) ? $options['config'] : [];
        $mailerConfig = (!empty($options) && isset($options['mailerConfig'])) ? $options['mailerConfig'] : [];
        $payload = (!empty($options) && isset($options['payload'])) ? $options['payload'] : [];
        $this->mailerConfig = array_merge($mailerConfig, $this->mailerConfig);
        $channelInstance = $this->getChannel($typeChannel, $configuration);
        $notification = new Notifications();
        $notification->setAttributes([
            'type' => $typeChannel,
            'message' => $payload['message'],
            'title' => $payload['title'],
            'additional_information' => $payload['additional_information']
        ]);
        $recipient = new NotificationRecipients([
            'recipient' => $payload['recipient'],
            'status' => 'P'
        ]);

        $notification->trigger(BaseActiveRecord::EVENT_BEFORE_INSERT);

        if ($notification->save()) {
            $recipient->notification_id = $notification->primaryKey;
            if ($recipient->save() && $channelInstance->send($notification)) {
                $response = true;
            }
        }

        // dd($notification->attributes);

        return $response;
    }

    /**
     * Permite el envio de las notificaciones pendientes
     *
     * @return void
     */
    public function sendAll($types)
    {
        $result = true;
        $models = Notifications::findBySql("SELECT nti.*
            FROM public.notifications nti
            inner join notification_recipients ntr on nti.notification_id = ntr.notification_id
            where ntr.status = :status AND nti.type IN (:types)", 
            [
                ':status' => NotificationRecipients::STATUS_PENDING,
                ':types' => implode(',', $types)
            ])->all();
        foreach ($models as $model) {
            $channel = $this->getChannel($model->type);
            $result &= $channel->send($model);
        }

        return $result;
    }
}
