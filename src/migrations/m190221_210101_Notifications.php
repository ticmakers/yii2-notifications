<?php
namespace ticmakers\notifications\migrations;
class m190221_210101_Notifications extends \yii\db\Migration
{
    public $tableName = 'notifications';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('notifications', [
            'notification_id' => $this->primaryKey(),
            'type' => $this->char(1)->notNull(),
            'title' => $this->string(128),
            'message' => $this->text()->notNull(),
            'small_icon' => $this->string(64),
            'big_icon' => $this->string(64),
            'additional_information' => $this->text(),
            'buttons' => $this->text(),
            'generated_from' => $this->string(64),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'type', "Type notification (W: Web notification; E: Email; M: Text message; P: Push notification)");
        $this->addCommentOnColumn($this->tableName, 'title', "Notification's title in case you apply how in emails");
        $this->addCommentOnColumn($this->tableName, 'message', "Notification's message");
        $this->addCommentOnColumn($this->tableName, 'small_icon', "Small icon that appears on the left side of push notifications");
        $this->addCommentOnColumn($this->tableName, 'big_icon', "Large icon that appears in push notifications");
        $this->addCommentOnColumn($this->tableName, 'additional_information', "Additional information used to transmit data to the application (Not visible by the user). It can be any text like a json or fix");
        $this->addCommentOnColumn($this->tableName, 'buttons', "Array or json with the buttons (identifier, name, icon, action, etc) that will appear in the notification");
        $this->addCommentOnColumn($this->tableName, 'generated_from', "Specifies the section or module of the system from which the notification was generated");

        $this->addCommentOnColumn($this->tableName, 'active', "Indicates whether the record is active or not");
        $this->addCommentOnColumn($this->tableName, 'created_by', "User's id who created the record");
        $this->addCommentOnColumn($this->tableName, 'created_at', "Date and time the record was created");
        $this->addCommentOnColumn($this->tableName, 'updated_by', "Last user's id who modified the record");
        $this->addCommentOnColumn($this->tableName, 'updated_at', "Date and time of the last modification of the record");

        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.active\"
                        CHECK (active IN ('Y', 'N'))");
        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.type\"
                        CHECK (type IN ('W', 'E', 'M', 'P'))");

        $this->addCommentOnTable($this->tableName, "System's notifications");

    }

    public function down()
    {
        $this->dropTable('notifications');
    }
}
