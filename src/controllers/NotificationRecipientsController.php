<?php
namespace ticmakers\notifications\controllers;

use app\models\app\Notifications;
use Yii;
use yii\web\Response;
use ticmakers\core\base\Controller;
use ticmakers\notifications\models\app\NotificationRecipients;

/**
 * Controlador NotificationRecipientsController implementa las acciones para el CRUD de el modelo NotificationRecipients.
 *
 * @package ticmakers\notifications
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class NotificationRecipientsController extends Controller
{
    public $isModal = true;
    public $modelClass = \ticmakers\notifications\models\app\NotificationRecipients::class;
    public $searchModelClass = \ticmakers\notifications\models\searchs\NotificationRecipients::class;


    public function actions()
    {
        $parentActions = parent::actions();
        $parentActions['create']['messageOnSuccess'] = Yii::t($this->module->id, 'It was created successfully.');
        $parentActions['update']['messageOnSuccess'] = Yii::t($this->module->id, 'It was updated successfully.');
        $parentActions['create']['beforeSave'] = function($model){
            $model->status = Notifications::STATUS_PENDING;
        };
        
        return $parentActions;
    }

   /*  public function actionCreate()
    {
        $instance = Yii::createObject($this->modelClass);
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            $instance->setAttributes($paramsGet);
        }
        if ($instance->load(Yii::$app->request->post())) {
            $instance->status = 'P';
            if ($instance->save()) {
                $res['state'] = 'success';
                $res['message'] = Yii::t(
                    $this->module->id,
                    'It was created successfully.'
                );
            } else {
                $res['state'] = 'error';
                $res['message'] = \yii\helpers\Html::errorSummary($instance);
                $res['error'] = ActiveForm::validate($instance);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax('_form', [
                'model' => $instance
            ]);
        }
    }
 */
    /**
     * Permite realizar el borrado lógico del registro (activo)
     *
     * @param integer $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $instance = $this->findModel($id);
        $instance->{$instance::STATUS_COLUMN} = $instance::STATUS_INACTIVE;
        if ($instance->save(false)) {
            Yii::$app->message::setMessage(
                Yii::$app->message::TYPE_SUCCESS,
                Yii::t($this->module->id, 'It is successfully removed.')
            );
        } else {
            Yii::$app->message::setMessage(
                Yii::$app->message::TYPE_DANGER,
                Yii::t($this->module->id, 'An error occurred while trying to delete.')
            );
        }
        return $this->redirect([
            'manage/update',
            'id' => $instance->notification_id,
            'tab' => 2
        ]);
    }
}
