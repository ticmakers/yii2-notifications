<?php

namespace ticmakers\notifications\controllers;

use Yii;

/**
 * Controlador DefaultController implementa las acciones para el CRUD de el modelo Notifications.
 *
 * @package ticmakers\notifications\controllers\ManageController
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class ManageController extends \ticmakers\core\base\Controller
{
    public $formInTabs = true;
    public $tabsConfig = [
        'tab1' => [
            'label' => 'Information',
            'viewNameTpl' => '_information_tpl',
            'modelsRender' => [
                'model'
            ],
            'hasPrevious' => false,
            'hasNext' => true,
            'saveAndContinue' => true
        ],
        'tab2' => [
            'label' => 'Recipients',
            'modelKey' => 'recipient',
            'hasPrevious' => true,
            'hasNext' => false,
            'disabledOnCreate' => true,
            'titleInGrid' => false
        ]
    ];

    public $modelClass = [
        'model' => [
            'class' => \ticmakers\notifications\models\app\Notifications::class,
            'isPrimary' => true
        ],
        'recipient' => [
            'class' => \ticmakers\notifications\models\app\NotificationRecipients::class,
            'searchClass' => \ticmakers\notifications\models\searchs\NotificationRecipients::class,
            'redisConfig' => [
                'moduleId' => 'notifications',
                'controllerId' => 'notification-recipients',
            ],
            'renderGrid' => true,
            'isRedis' => false,
            'isMultiple' => true,
            'relatedModels' => [
                'model' => [
                    'isModelLoad' => true, //Solo se debe definir un modelo de carga
                    'relationName' => 'notificationRecipients',
                    'relatedFields' => [
                        'notification_id' => 'notification_id'
                    ]
                ]
            ]
        ]
    ];
    public $searchModelClass = \ticmakers\notifications\models\searchs\Notifications::class;

    /**
     * @inheritDoc
     */
    public function actions()
    {
        $parentActions = parent::actions();
        $parentActions['create']['routeRedirect'] = function ($model) {
            $returnUrl = 'index';

            if ($returnTab = Yii::$app->request->post('SaveAndReturnTab')) {
                if (is_array($model)) {
                    $model = $model['model'];
                }
                $returnUrl = ['/notifications/manage/update', 'id' => $model->notification_id, '#' => $returnTab];
            }
            return $returnUrl;
        };
        $parentActions['create']['messageOnSuccess'] = Yii::t('app', 'The notification was created successfully');
        return $parentActions;
    }

    /**
     * Realiza la ejecución de las notificaciones
     *
     * @param $id
     */
    public function actionPlay($id)
    {
        $itsOk = true;
        $message = Yii::t(
            $this->module->id,
            'The notification was sent successfully.'
        );
        $notification = Notifications::findOne($id);
        if (!empty($notification)) {
            $channelInstance = $this->module->getChannel($notification->type);
            $itsOk = $channelInstance->send($notification);
            if (!$itsOk) {
                $message = Yii::t(
                    $this->module->id,
                    'Ha ocurrido un error enviando la notificación a alguno de los destinatarios.'
                );
            }
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $returnValue = [
                'state' => $itsOk ? 'success' : 'error',
                'message' => $message
            ];
        } else {
            Yii::$app->message::setMessage(
                $itsOk
                    ? Yii::$app->message::TYPE_SUCCESS
                    : Yii::$app->message::TYPE_DANGER,
                $message
            );
            $returnValue = $this->redirect('index');
        }
        return $returnValue;
    }
}
