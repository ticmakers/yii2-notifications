<?php

namespace ticmakers\notifications\components;

use ticmakers\notifications\Module;
/**
 * Modelo base
 *
 * @package ticmakers
 * @subpackage notifications
 * @category Components
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Model extends \ticmakers\core\base\Model
{
   

    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->module = Module::getInstance();
    }
}
