<?php

namespace ticmakers\notifications\components;

use ticmakers\notifications\models\base\Notifications;
use Yii;
use ticmakers\notifications\Module;

/**
 * Clase abstracta para el envío por medio de un servicio.
 *
 * @package ticmakers
 * @subpackage notifications\components
 * @category components
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
abstract class Channel extends \yii\base\BaseObject
{
    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Tipo de notificación a ser procesada por el canal
     *
     * @var string
     */
    public $type;

    /**
     * Mensaje de error en caso de haberlo.
     *
     * @var string
     */
    public $errorMessage = '';

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->module = Module::getInstance();
        if (is_null($this->type)) {
            throw new \yii\base\InvalidConfigException(
                Yii::t(
                    $this->module->id,
                    "It's necessary to define the type of notification."
                )
            );
        }
    }

    /**
     * Realiza el envío de la notificación por el servicio indicado.
     *
     * @return void
     */
    abstract public function send(Notifications $notification);
}
