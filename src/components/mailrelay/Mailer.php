<?php

namespace ticmakers\notifications\components\mailrelay;

class Mailer extends \yii\mail\BaseMailer
{
    public $apiKey;
    public $hostName;
    public $html;

    /**
     * @var string message default class name.
     */
    public $messageClass = 'ticmakers\notifications\components\mailrelay\Message';

    /**
     * Undocumented function
     *
     * @param [type] $message
     * @return void
     */
    protected function sendMessage($message)
    {
        $address = $message->getTo();
        if (is_array($address)) {
            $address = implode(', ', array_keys($address));
        }
        Yii::info(
            'Sending email "' .
                $message->getSubject() .
                '" to "' .
                $address .
                '"',
            __METHOD__
        );

        return $message->send($this);
    }
}
