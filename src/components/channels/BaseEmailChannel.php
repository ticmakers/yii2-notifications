<?php

namespace ticmakers\notifications\components\channels;

use ticmakers\notifications\models\base\Notifications;
use ticmakers\notifications\models\base\NotificationRecipients;
use Yii;

/**
 * Canal base para el envío de notificaciones a través de correos electrónico.
 *
 * @package ticmakers
 * @subpackage notifications\components\channels
 * @category Channel
 *
 * @property string $address Dirección de la organización, (Esto es para los parámetros de plantilla por defecto).
 * @property string $company Nombre de la organización, (Esto es para los parámetros de plantilla por defecto).
 * @property string $fromEmail Dirección de correo por defecto para el remitente del correo.
 * @property string $htmlLayout Layout a partir del cual es creado el correo electrónico.
 * @property string $logo URI de la imagén que quiere utilizarse en la plantilla por defecto como logo.
 * @property string $template Plantilla por defecto para la creación del correo.
 * @property string $type Tipo de notificación a la cual atiende este canal.
 * @property array|string $urlCompany Define la URL de la organización, esto puede poseer dos valores posibles que son:
 * - La url directamente
 * - Un arreglo donde la posición 'label' define el texto mostrado y la posición 'url' define la url.
 * @property string $mailer Proveedor de servicios para el envío de correos.
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 **/
class BaseEmailChannel extends \ticmakers\notifications\components\Channel
{
    public $address = 'Cra 7 # 65-82 Casa 2 Barrio Arkacentro, Ibagué, Tolima';
    public $company = 'TIC Makers';
    public $fromEmail = 'no-reply@ticmakers.com';
    public $htmlLayout = '@ticmakers/notifications/mail/layout/default';
    public $logo = '@ticmakers/notifications/assets/images/default-logo.png';
    public $template = '@ticmakers/notifications/mail/default-template';
    public $type = 'E';
    public $urlCompany = [
        'label' => 'TIC Makers',
        'url' => 'http://www.ticmakers.com'
    ];

    protected $mailer;

    /**
     * Retorna un arreglo con todos los datos necesarios para armar el email,
     * esto incluye los parámetros adicionales.
     *
     * @param Notifications $notification Notificación que suministra la información.
     * @return array
     */
    public function getParamsEmail(
        Notifications $notification,
        NotificationRecipients $recipent
    ) {
        return array_merge(self::getAdditionalInformation($notification), [
            'notification' => $notification,
            'recipent' => $recipent,
            'address' => $this->address,
            'company' => $this->company,
            'urlCompany' => $this->urlCompany,
            'logo' => Yii::getAlias($this->logo)
        ]);
    }

    /**
     * Método send por defecto.
     */
    public function send(Notifications $notification)
    {
        return true;
    }

    /**
     * Retorna el un arreglo con los datos adicionales según el objeto notification.
     *
     * @param Notifications $notification Notificación que suministra la información.
     * @return array
     */
    public static function getAdditionalInformation(Notifications $notification)
    {
        $aditionalParams = json_decode(
            $notification->additional_information,
            true
        );
        if (json_last_error() != 0) {
            $aditionalParams = [];
        }
        return $aditionalParams;
    }
}
