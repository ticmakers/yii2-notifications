<?php

namespace ticmakers\notifications\components\channels;

use Yii;
use ticmakers\notifications\models\base\Notifications;
use ticmakers\notifications\models\base\NotificationRecipients;
use ticmakers\notifications\Module;

/**
 * Canal para manejar el envío de notificaciones por correo electrónico via Email.
 *
 * @package ticmakers
 * @subpackage notifications\components\channels
 * @category components
 *
 * @property string $errorMessage Mensaje de error a mostrar en caso de haberlo.
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class EmailChannel extends
    \ticmakers\notifications\components\channels\BaseEmailChannel
{
    public $errorMessage = "";
    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $mailConfiguration = $this->module->mailerConfig;
        if ($this->validateConfigurations()) {
            $this->mailer = Yii::createObject($mailConfiguration);
        }
    }

    /**
     * Método para el envío de los datos
     *
     * @param Notifications $notification
     * @return void
     */
    public function send(Notifications $notification)
    {
        $isOk = false;
        if (!$this->validateConfigurations()) {
            foreach (
                $notification->notificationRecipients
                as $notificationRecipent
            ) {
                $notificationRecipent->status = 'F';
                $notificationRecipent->status_information = $this->errorMessage;
                $notificationRecipent->save(false);
            }
        } else {
            $isOk = true;
            foreach (
                $notification->notificationRecipients
                as $notificationRecipent
            ) {
                if ($notificationRecipent->status != 'S') {
                    if ($notificationRecipent->validate()) {
                        $this->mailer->htmlLayout = $this->htmlLayout;
                        $mailInstance = $this->mailer
                            ->compose(
                                [
                                    'html' => $this->template
                                ],
                                $this::getParamsEmail(
                                    $notification,
                                    $notificationRecipent
                                )
                            )
                            ->setFrom($this->fromEmail)
                            ->setTo($notificationRecipent->recipient)
                            ->setSubject($notification->title);
                        try {
                            $mailInstance->send();
                            $notificationRecipent->load(
                                [
                                    'status' => 'S',
                                    'status_information' => ''
                                ],
                                ''
                            );
                        } catch (\Swift_SwiftException $ex) {
                            $isOk = false;
                            $notificationRecipent->load(
                                [
                                    'status' => 'F',
                                    'status_information' => $ex->getMessage()
                                ],
                                ''
                            );
                        }
                        $notificationRecipent->save(false);
                    } else {
                        $errors = $notificationRecipent->getErrors('recipient');
                        $notificationRecipent->load(
                            [
                                'status' => 'F',
                                'status_information' => $errors[0]
                            ],
                            ''
                        );
                        $notificationRecipent->save(false);
                    }
                }
            }
        }
        return $isOk;
    }

    /**
     * Valida las configuraciones para el canal.
     *
     * @param booelan $throwError Indica si se quiere lanzar el error como una excepción en la aplicación.
     * @return boolean
     */
    public function validateConfigurations($throwError = false)
    {
        $mailConfiguration = $this->module->mailerConfig;
        $isValid = true;
        if (
            !(
                !is_null($mailConfiguration) &&
                isset($mailConfiguration['class'])
            )
        ) {
            $isValid = false;
            $this->errorMessage = Yii::t(
                $this->module->id,
                "It is necessary to define the configuration of the mail component and the instantiator class of this."
            );
        }
        if ($throwError && !$isValid) {
            Yii::$app->session->setFlash(
                $this->module->id . '-error',
                $this->errorMessage
            );
        }
        return $isValid;
    }
}
