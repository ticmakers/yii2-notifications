<?php

namespace ticmakers\notifications\components\channels;

use Yii;
use ticmakers\notifications\models\base\Notifications;

/**
 * Canal para manejar el envío de notificaciones por correo electrónico via OnesignalChannel.
 *
 * @package ticmakers
 * @subpackage notifications\components\channels
 * @category components
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class WebChannel extends \ticmakers\notifications\components\Channel
{
    /**
     * Instancia de onesignal.
     *
     * @var [type]
     */
    protected $dialogConfig;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $type = 'W';

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        // if (is_null($this->module->onesignalConfig)) {
        //     throw new \yii\base\InvalidConfigException(
        //         Yii::t(
        //             $this->module->id,
        //             "It's necessary to define the configuration to the onesignal component."
        //         )
        //     );
        // }
        // $this->onesignal = Yii::createObject($this->module->onesignalConfig);
    }

    /**
     * Método para el envío de los datos
     *
     * @param Notifications $notifications
     * @return void
     */
    public function send(Notifications $notifications)
    {
    }
}
