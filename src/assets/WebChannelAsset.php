<?php
namespace ticmakers\notifications\assets;

/**
 * WebChannelAssets Permite la carga de los plugins y dependencias necesarias para el canal de notificaciones Web
 *
 * @package ticmakers\notifications
 * @category Assets
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author  Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author  kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class WebChannelAsset extends AssetBundle
{
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yiiassets\bootbox\BootBoxAsset',
    ];
}
