<?php

namespace ticmakers\notifications\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ticmakers\notifications\models\app\Notifications as NotificationsModel;

/**
 * Esta clase representa las búsqueda para el modelo `ticmakers\notifications\models\base\Notifications`.
 *
 * @package ticmakers/notifications
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class Notifications extends NotificationsModel
{
    /**
     * Número de notificaciones pendientes
     *
     * @var int
     */
    public $pending;

    /**
     * Número de notificaciones fallidas
     *
     * @var int
     */
    public $failed;

    /**
     * Número de notificaciones enviadas
     *
     * @var int
     */
    public $sent;

    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['notification_id', 'created_by', 'updated_by', 'pending', 'failed', 'sent'], 'integer'],
            [['type', 'title', 'message', 'small_icon', 'big_icon', 'additional_information', 'buttons', 'generated_from', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificationsModel::find()->alias('nof');
        $query->select("
            nof.*,
            (SELECT count(nt1.notification_recipient_id) FROM notification_recipients nt1 WHERE nt1.active='Y' AND nt1.status='P' AND nt1.notification_id = nof.notification_id) as pending,
            (SELECT count(nt2.notification_recipient_id) FROM notification_recipients nt2 WHERE nt2.active='Y' AND nt2.status='S' AND nt2.notification_id = nof.notification_id) as sent,
            (SELECT count(nt3.notification_recipient_id) FROM notification_recipients nt3 WHERE nt3.active='Y' AND nt3.status='F' AND nt3.notification_id = nof.notification_id) as failed");
        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ],
            ]
        ]);

        $dataProvider->sort->attributes = array_merge($dataProvider->sort->attributes,
        [
            'pending' => [
                'asc' => [ 'pending' => SORT_ASC ],
                'desc' => [ 'pending' => SORT_DESC ]
            ],
            'sent' => [
                'asc' => [ 'sent' => SORT_ASC ],
                'desc' => [ 'sent' => SORT_DESC ]
            ],
            'failed' => [
                'asc' => [ 'failed' => SORT_ASC ],
                'desc' => [ 'failed' => SORT_DESC ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'nof.notification_id' => $this->notification_id,
            'nof.created_by' => $this->created_by,
            'DATE(nof.created_at)' => $this->created_at,
            'nof.updated_by' => $this->updated_by,
            'DATE(nof.updated_at)' => $this->updated_at,
            "(SELECT count(nt4.notification_recipient_id) FROM notification_recipients nt4 WHERE nt4.active='Y' AND nt4.status='P' AND nt4.notification_id = nof.notification_id)" => $this->pending,
            "(SELECT count(nt5.notification_recipient_id) FROM notification_recipients nt5 WHERE nt5.active='Y' AND nt5.status='S' AND nt5.notification_id = nof.notification_id)" => $this->sent,
            "(SELECT count(nt6.notification_recipient_id) FROM notification_recipients nt6 WHERE nt6.active='Y' AND nt6.status='F' AND nt6.notification_id = nof.notification_id)" => $this->failed
        ]);

        $query->andFilterWhere(['ilike', 'nof.type', $this->type])
            ->andFilterWhere(['ilike', 'nof.title', $this->title])
            ->andFilterWhere(['ilike', 'nof.message', $this->message])
            ->andFilterWhere(['ilike', 'nof.small_icon', $this->small_icon])
            ->andFilterWhere(['ilike', 'nof.big_icon', $this->big_icon])
            ->andFilterWhere(['ilike', 'nof.additional_information', $this->additional_information])
            ->andFilterWhere(['ilike', 'nof.buttons', $this->buttons])
            ->andFilterWhere(['ilike', 'nof.generated_from', $this->generated_from])
            ->andFilterWhere(['like', 'nof.active', $this->active]);

        return $dataProvider;
    }
}
