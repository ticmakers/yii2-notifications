<?php

namespace ticmakers\notifications\models\app;

use kartik\grid\GridView;
use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * Éste es el modelo para la tabla "notification_recipients".
 * System's notifications recipients
 *
 * @package ticmakers\notifications\models\app\NotificationRecipients 
 *
 * @property integer $notification_recipient_id Records's unique identifier
 * @property integer $notification_id Notification linked to the recipient
 * @property string $recipient Destination to which the notification will arrive. It can be an email, a cell phone number, a device identifier, etc.
 * @property integer $user_id System's user associated with the recipient. This only applies in the case that the recipient has a user within the system
 * @property string $status Notification's status for the recipient. Can take the values ​​P: Pending send; S: Successful send; F: Failed send
 * @property string $status_information Additional information about the state. Example, in case of failure here goes the description of the fault.
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Notifications $notification Datos relacionados con modelo "Notifications"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 */
class NotificationRecipients extends \ticmakers\notifications\models\base\NotificationRecipients
{
    /**
     * @inheritDoc
     */
    public $includeActionColumns = false;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {

        
        $formColumns = [
            'status' => [
                'attribute' => 'status',
                'widget' => [
                    "class" => "kartik\widgets\Select2",
                    "data" => Notifications::getStatus(),
                    "options" => [
                        "placeholder" => Yii::$app->strings::getTextEmpty(),
                    ]
                ],
                'render' => ['C', 'U'],
            ],
            'user_id' => [
                'attribute' => 'user_id',
                'widget' => [
                    "class" => "kartik\widgets\Select2",
                    "data" => Yii::$app->userHelper::getUsers(),
                    "options" => [
                        "placeholder" => Yii::$app->strings::getTextEmpty(),
                    ]
                ],
                'render' => ['C', 'U'],
            ]
        ];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            'updated_at' => [
                'attribute' => 'updated_at',
                'label' => Yii::t($this->module->id,'Last attempt to send'),
                'filterType' => GridView::FILTER_DATE,
            ],
            'status' => [
                'attribute' => 'status',
                'label' => Yii::t($this->module->id, 'Status'),
                'value' => function ($model) {
                    return Notifications::getStatus($model->status);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Notifications::getStatus(),
                'filterInputOptions' => [
                    'placeholder' => Yii::$app->strings::getTextAll()
                ]
            ]
        ];
        $gridColumns['actions'] = [
            'class' => \ticmakers\core\base\ActionColumn::class,
            'controller' => 'notification-recipients',
            'dynaModalId' => $id,
            'template' => '{update} {delete} {restore}',
            'visibleButtons' => [
                'play' => function($model){
                    return ($model->active == $model::STATUS_ACTIVE && $model->type != static::TYPE_WEB);
                },
                'delete' => function($model){
                    return ($model->active == $model::STATUS_ACTIVE);
                },
                'restore' => function($model){
                    return ($model->active == $model::STATUS_INACTIVE);
                },
            ],
            'buttons' => [
                'play' => function ($url, $model) use ($id) {
                    return Yii::$app->html::a('<span class="fa fa-play"></span>', $url, [
                        'title' => Yii::t($this->module->id, 'Url'),
                        'data-gridview' => $id,
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    $url = ['notification-recipients/delete', 'id' => $model->primaryKey];
                    $options = [
                        'data-pjax' => 0,
                        'data-method' => 'post',
                        'data-confirm' => Yii::t($this->module->id, 'Are you sure you want to delete this item?'),
                        'data-pjax' => 0,
                        'title' => Yii::t($this->module->id, 'Delete')
                    ];
                    return Yii::$app->html::a(Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_TRASH), Url::to($url), $options);
                
                },
                'restore' => function ($url, $model, $key) {
                        $url = ['notification-recipients/restore', 'id' => $model->primaryKey];
                        $options = [
                            'data-pjax' => 0,
                            'data-method' => 'post',
                            'data-confirm' => Yii::t($this->module->id, 'Are you sure you want to restore this item?'),
                            'title' => Yii::t($this->module->id, 'Restore')
                        ];
                      
                        return Yii::$app->html::a('<span class="fa fa-redo"></span>', Url::to($url), $options);
                }
            ]
        ];
        $parentGridColumns = parent::gridColumns();
        // $parentGridColumns['']['label'] = Yii::t($this->module->id,''); 
        unset($parentGridColumns['notification_id'], $parentGridColumns['user_id']);
        return Yii::$app->arrayHelper::merge($parentGridColumns, $gridColumns);
    }
}
