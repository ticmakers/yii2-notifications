<?php

namespace ticmakers\notifications\models\app;

use kartik\grid\GridView;
use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * Éste es el modelo para la tabla "notifications".
 * System's notifications
 *
 * @package ticmakers\notifications\models\app 
 *
 * @property integer $notification_id 
 * @property string $type Type notification (W: Web notification; E: Email; M: Text message; P: Push notification)
 * @property string $title Notification's title in case you apply how in emails
 * @property string $message Notification's message
 * @property string $small_icon Small icon that appears on the left side of push notifications
 * @property string $big_icon Large icon that appears in push notifications
 * @property string $additional_information Additional information used to transmit data to the application (Not visible by the user). It can be any text like a json or fix
 * @property string $buttons Array or json with the buttons (identifier, name, icon, action, etc) that will appear in the notification
 * @property string $generated_from Specifies the section or module of the system from which the notification was generated
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property NotificationRecipients[] $notificationRecipients Datos relacionados con modelo "NotificationRecipients"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 */
class Notifications extends \ticmakers\notifications\models\base\Notifications
{
    const STATUS_PENDING = 'P';
    const STATUS_FAILED = 'F';
    const STATUS_SENT = 'S';
    const TYPE_EMAIL = 'E';
    const TYPE_PUSH = 'P';
    const TYPE_WEB = 'W';

    /**
     * Define si se incluye la columna de acciones del dynagrid
     * @var boolean
     */
    public $includeActionColumns = false;

    /**
     * Número de notificaciones pendientes
     *
     * @var int
     */
    public $pending;

    /**
     * Número de notificaciones fallidas
     *
     * @var int
     */
    public $failed;

    /**
     * Número de notificaciones enviadas
     *
     * @var int
     */
    public $sent;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return 'title';//parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $parentFormColums = parent::formColumns();

        $formColumns = [
            'type' => [
                'attribute' => 'type',
                'widget' => [
                    "class" => "kartik\widgets\Select2",
                    "data" => static::getTypes(),
                    "options" => [
                        "placeholder" => Yii::$app->strings::getTextEmpty(),
                    ]
                ],
                'render' => ['C', 'U'],
            ],
            'status' => [
                'attribute' => 'status',
                'widget' => [
                    "class" => "kartik\widgets\Select2",
                    "data" => static::getStatus(),
                    "options" => [
                        "placeholder" => Yii::$app->strings::getTextEmpty(),
                    ]
                ],
                'render' => ['C', 'U'],
            ]
        ];

        $parentFormColums['generated_from']['render'] = ['C', 'U'];
        $parentFormColums['buttons']['render'] = ['C', 'U'];
        $parentFormColums['message']['render'] = ['C', 'U'];
        $parentFormColums['additional_information']['render'] = ['C', 'U'];
        $parentFormColums['big_icon']['render'] = ['C', 'U'];
        $parentFormColums['small_icon']['render'] = ['C', 'U'];
        return Yii::$app->arrayHelper::merge($parentFormColums, $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $parentGridColumns = parent::gridColumns();
        $gridColumns = [
            'type' => [ 
                'attribute' => 'type',
                'value' => function($model){
                    return static::getTypes($model->type);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => static::getTypes(),
                'filterInputOptions' => [
                    'placeholder' => Yii::$app->strings::getTextAll()
                ]
            ],
            'updated_at' => [
                'attribute' => 'updated_at',
                'label' => Yii::t('notifications','Date'),
                'filterType' => GridView::FILTER_DATE,
            ],
            'pending' => [
                'attribute' => 'pending',
                'label' => Yii::t('notifications','Pending')
            ],
            'failed' => [
                'attribute' => 'failed',
                'label' => Yii::t('notifications','Failed')
            ],
            'sent' => [
                'attribute' => 'sent',
                'label' => Yii::t('notifications','Sent')
            ]
        ];

        unset($parentGridColumns['big_icon'], $parentGridColumns['small_icon'], $parentGridColumns['generated_from']);
        $gridColumns['actions'] = [
            'class' => \ticmakers\core\base\ActionColumn::class,
            'controller' => 'manage',
            'dynaModalId' => $id,
            'template' => '{play} {update} {delete} {restore}',
            'visibleButtons' => [
                'play' => function($model){
                    return ($model->active == $model::STATUS_ACTIVE && $model->type != static::TYPE_WEB);
                }
            ],
            'buttons' => [
                'play' => function ($url, $model) use ($id) {
                    return Yii::$app->html::a('<span class="fa fa-play"></span>', $url, [
                        'title' => Yii::t('app', 'Url'),
                        'data-gridview' => $id,
                    ]);
                }
            ]
        ];
        return Yii::$app->arrayHelper::merge($parentGridColumns, $gridColumns);
    }

    /**
     * Obtener el listado de estados de las notificaciones
     *
     * @return array | $statusList: listado de estados de las notificaciones
     */
    public static function getStatus($statusCode = null)
    {
        $status = [
            static::STATUS_PENDING => Yii::t('notifications', 'Pending'),
            static::STATUS_FAILED => Yii::t('notifications', 'Failed'),
            static::STATUS_SENT => Yii::t('notifications', 'Sent')
        ];
        return !empty($statusCode) ? $status[$statusCode]: $status;

    }

    /**
     * Obtener listado de tipos de notificación con su respectiva traducción
     *
     * @return array | $typeList: listado de tipos de notificación
     */
    public static function getTypes($typeCode = null)
    {
        $types =  [
            static::TYPE_EMAIL => Yii::t('notifications', 'Email'),
            static::TYPE_PUSH => Yii::t('notifications', 'Push notification'),
            static::TYPE_WEB => Yii::t('notifications','Web')
        ];
        return !is_null($typeCode)? $types[$typeCode]: $types;
    }
}
