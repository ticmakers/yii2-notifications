<?php

namespace ticmakers\notifications\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Éste es el modelo para la tabla "notification_recipients".
 * System's notifications recipients
 *
 * @package ticmakers/notifications
 * @subpackage models/base
 * @category models
 *
 * @property integer $notification_recipient_id Records's unique identifier
 * @property integer $notification_id Notification linked to the recipient
 * @property string $recipient Destination to which the notification will arrive. It can be an email, a cell phone number, a device identifier, etc.
 * @property integer $user_id System's user associated with the recipient. This only applies in the case that the recipient has a user within the system
 * @property string $status Notification's status for the recipient. Can take the values ​​P: Pending send; S: Successful send; F: Failed send
 * @property string $status_information Additional information about the state. Example, in case of failure here goes the description of the fault.
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Fecha y hora en que se creó el registro
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Notifications $notification Datos relacionados con modelo "Notifications"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzmán Delgidillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 2.0.0
 */
class NotificationRecipients extends \ticmakers\notifications\components\Model
{
    const STATUS_PENDING = 'P';
    const STATUS_FAILED = 'F';
    const STATUS_SUCCESSFUL = 'S';
    /**
     * Se sobreescribe para guardar datos adicionales
     *
     * @param boolean $runValidation
     * @param array $attributeNames
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        if ($runValidation && !$this->validate()) {
            return false;
        }
        try {
            $this->{self::CREATED_BY_COLUMN} = $this::getUserId();
            $this->{self::UPDATED_BY_COLUMN} = $this::getUserId();
            parent::save(false, $attributeNames);
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return true;
    }

    /**
     * Retorna el identificador del usuario que crea el recurso.
     *
     * @return int
     */
    public static function getUserId()
    {
        $userId = self::DEFAULT_USER_ID;
        if (
            !(Yii::$app->controller instanceof \yii\console\Controller) &&
            !Yii::$app->user->isGuest
        ) {
            $userId = Yii::$app->user->id;
        }
        return $userId;
    }

    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'notification_recipients';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['notification_id', 'recipient', 'status'], 'required'],
            [['notification_id', 'user_id', 'updated_by'], 'integer'],
            [['status_information'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['recipient'], 'string', 'max' => 128],
            [['status'], 'string', 'max' => 1],
            [['status'], 'default', 'value' => 'P'],
            [
                ['recipient'],
                'email',
                'when' => function ($model) {
                    return !$model->isNewRecord &&
                        $model->notification->type == 'E';
                }
            ],
            [
                ['notification_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Notifications::className(),
                'targetAttribute' => ['notification_id' => 'notification_id']
            ]
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'notification_recipient_id' => Yii::t($this->module->id, 'Code'),
            'notification_id' => Yii::t($this->module->id, 'Notification'),
            'recipient' => Yii::t($this->module->id, 'Recipient'),
            'user_id' => Yii::t($this->module->id, 'User'),
            'status' => Yii::t($this->module->id, 'Status'),
            'status_information' => Yii::t(
                $this->module->id,
                'Status Information'
            ),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At')
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'notification_recipient_id' => Yii::t(
                $this->module->id,
                'Records\'s unique identifier'
            ),
            'notification_id' => Yii::t(
                $this->module->id,
                'Notification linked to the recipient'
            ),
            'recipient' => Yii::t(
                $this->module->id,
                'Destination to which the notification will arrive. It can be an email, a cell phone number, a device identifier, etc.'
            ),
            'user_id' => Yii::t(
                $this->module->id,
                'System\'s user associated with the recipient. This only applies in the case that the recipient has a user within the system'
            ),
            'status' => Yii::t(
                $this->module->id,
                'Notification\'s status for the recipient. Can take the values ​​P: Pending send; S: Successful send; F: Failed send'
            ),
            'status_information' => Yii::t(
                $this->module->id,
                'Additional information about the state. Example, in case of failure here goes the description of the fault.'
            ),
            'active' => Yii::t(
                $this->module->id,
                'Indicates whether the record is active or not'
            ),
            'created_by' => Yii::t(
                $this->module->id,
                'User\'s id who created the record'
            ),
            'created_at' => Yii::t(
                $this->module->id,
                'Fecha y hora en que se creó el registro'
            ),
            'updated_by' => Yii::t(
                $this->module->id,
                'Last user\'s id who modified the record'
            ),
            'updated_at' => Yii::t(
                $this->module->id,
                'Date and time of the last modification of the record'
            )
        ];
        return !is_null($attribute) && isset($helps[$attribute])
            ? $helps[$attribute]
            : $helps;
    }

    /**
     * Definición de la relación con el modelo "Notification".
     *
     * @return \ticmakers\notifications\models\base\Notification
     */
    public function getNotification()
    {
        return $this->hasOne(Notifications::className(), [
            'notification_id' => 'notification_id'
        ]);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|NotificationRecipients[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()
            ->where($attributes)
            ->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map(
                $query,
                'notification_recipient_id',
                self::getNameFromRelations()
            );
        }
        return $query;
    }
}
