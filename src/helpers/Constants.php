<?php

namespace ticmakers\notifications\helpers;

use Yii;

/**
 * Helper para el almacenado de todas las constantes del sistema
 *
 * @package app
 * @subpackage helpers
 * @category helpers
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @version 0.0.1
 * @copyright TicMakers 2019
 */
class Constants
{
    const TYPE_NOTIFICATION = [
        'E' => 'Email',
        'P' => 'Push notification'
    ];

    const STATUS_NOTIFICATION = [
        'P' => 'Pending',
        'F' => 'Failed',
        'S' => 'Successful'
    ];
}
