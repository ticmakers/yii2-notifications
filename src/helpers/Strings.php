<?php

namespace ticmakers\notifications\helpers;

use Yii;

/**
 * Clase Helper para ayudar a administrar los textos de la aplicación.
 *
 * @package ticmakers
 * @subpackage notifications/helpers
 * @category Helpers
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sacnhez@ticmakers.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Strings extends \ticmakers\core\helpers\StringsHelper
{
    //Estados para condiciones
    const SI = 'Y';
    const NO = 'N';

    /**
     * Método para retornar los valores para condición.
     *
     * @param string $index Indice para retornar su valor default NULL
     * @return array
     */
    public static function getCondition($index = null)
    {
        $res = [
            static::SI => Yii::t('app', 'Sí'),
            static::NO => Yii::t('app', 'No')
        ];
        return $index == null ? $res : $res[$index];
    }
}
