<?php

namespace ticmakers\notifications;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 *
 * @package ticmakers\yii2-notifications
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('notifications')) {
            $app->setModule('notifications', \ticmakers\notifications\Module::class);
        }

        if (!$app->has('onesignal')){
            $moduleNotification = $app->getModule('notifications');
            if(!empty($moduleNotification->onesignalConfig)){
                $app->set('onesignal', $moduleNotification->onesignalConfig);
            }
        }
        
        if ($app instanceof \yii\console\Application) {
        	$app->getModule('notifications')->controllerNamespace = 'ticmakers\notifications\commands';
        }
    }
}
