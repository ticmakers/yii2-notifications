<?php

namespace ticmakers\notifications\commands;

use Yii;
use ticmakers\notifications\models\base\Notifications;

class DefaultController extends \yii\console\Controller
{
    
    /**
     * Lanza la notificación según el Identificador pasado
     */
    public function actionPlay($id)
    {
        $notification = Notifications::findOne($id);
        if (!empty($notification)) {
            $channelInstance = $this->module->getChannel($notification->type);
            if (!empty($channelInstance)) {
                $channelInstance->send($notification);
            }
        }
    }
}