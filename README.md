# Yii2 Notifications.

## Introduction

It allows the administration of system notifications through external services such as email servers and push notifications.
## Installation

Using [Composer](https://getcomposer.org/)
```sh
$ composer require ticmakers/yii2-parameters
```

Applying migrations
```sh
$ php yii migrate --migrationPath=@vendor/ticmakers/yii2-parameters/migrations
```

Configuration required in web.php file:
```php
'modules'=>[
   'notifications' => [
        'class' => '\ticmakers\notifications\Module',
        'methodSendMail' => 'mailrelay',
        'mailrelayConfig' => [
            'apiKey' => 'ndkGoIhlOZ60dD9rNXXeB5kvPxxuiaVL46RiFzKT',
            'hostName' => 'ticmakers1.ip-zone.com'
        ],
        'mailerConfig' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.integraciones.ticmakers.com',
                'username' => 'noreply@integraciones.ticmakers.com',
                'password' => 'C.aT~vkZV-[p',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ]
            ],
        ],
        'onesignalConfig' => [
            'class' => '\rocketfirm\onesignal\OneSignal',
            'appId' => '71e80374-111e-4411-a5df-ff5073ef3060',
            'apiKey' => 'MGI5YTQxZmMtODY2My00OGE0LWE3NzMtNTIzNTYyMTM0MTZm',
        ],
    ]
 ],
```

These initial settings must be added for the module. The position **methodSendMail** indicates the type of service that will be used to send the emails, this can have two possible values: mailrelay and smtp.
Each type of associated service will have a configuration that must be provided in the following way:
* For **smtp**: In the position **mailerConfig** all the information of the mail server will be supplied as well as the connection driver to it, in our example we will have **Swiftmailer**, which provides functions for the sending of mails by **SMTP**.
* For **mailrelay**: En la posición **mailrelayConfig** se suministra tanto el **apikey** y el **host** que provee el servicio de [Mailrelay](https://mailrelay.com).

Another service that must be configured is the onesignal for handling push notifications. For this we use the onesignal service which will provide us with the application identifier or **appId** and the **apiKey**, this information will go in the **onesignalConfig** position as well as the driver for its connection which by default must be \rocketfirm\onesignal\OneSignal.

### Module architecture.

The module uses a simple architecture that is based on two components:
* **Notifications**: It's entities with the information stored in the table **notifications** of the database. The notifications provide the subject and content of the message plus some metadata necessary for the shipment.
* **Channels**: Channels are objects that provide the necessary interface for sending notifications, each of the channels corresponds to a service, in the case of mailrelay there is a channel **Mailrelay**.

### Advanced configurations.

In the configuration of the module you can also pass some other configurations, then we explain them to you.

| Property | Description | Default value |
| -------- | ----------- | ------------- |
| onesignalConfig | Configuration for the push notification service. | [] |
| mailerConfig | Configuration for the service of emails by SMTP. | [] |
| mailrelayConfig | Configuration for email service through Mailrelay. | [] |
| methodSendMail | Method of sending emails, this can be SMTP or Mailrelay. | 'smtp' |
| channels | Configuration of the default channels for the module, these can be modified in order to extend the functionality or create new channels. | {Check the following code for the table} |
| breadcrumbsBase | Arrange with the breadcrumbs that will be shown in the module. | null |
| controllerNamespace | Location of the controllers for the module. | 'ticmakers\notifications\controllers' |

```php
[
    'smtp' => [
        'class' => 'ticmakers\notifications\components\channels\EmailChannel'
    ],
    'onesignal' => [
        'class' => 'ticmakers\notifications\components\channels\OnesignalChannel'
    ],
    'mailrelay' => [
        'class' => 'ticmakers\notifications\components\channels\MailRelayChannel'
    ],
    'sms' => [
        'class' => 'ticmakers\notifications\components\channels\SMSChannel'
    ],
]
```

### License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause).
Created by: [**TIC Makers Developer Team**](http://www.ticmakers.com).