# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.1.0] - 2019-07-07
### Added
- Archivos CHANGELOG, CONTRIBUTING (.md) fueron añadidos al proyecto para ajustar a estructura de paquetes para PHP.
- Añadidos dos autores al archivo **composer.json**.

### Changed
- El texto de la licencia fue modificado para que correspondiera al tipo de licencia que aparece en el archivo **composer.json**.
- Se modificó la versión del paquete *ticmakers/yii2-base* requerido en el archivo **composer.json**.